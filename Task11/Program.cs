﻿using System;

namespace Task11
{
    //  Program for payment system
    class Program
    {
        static void Main(string[] args)
        {
            //  Pre-defined values for the users balance on the credit card, saving card and in cash
            Debit debit = new Debit();
            debit.balance = 500;
            Credit credit = new Credit();
            credit.limit = 1000;
            credit.balance = 0;
            Cash cash = new Cash();
            cash.balance = 583;
            int price = 200;

            Console.WriteLine("This item is " + price + " kroner, how would you like to pay?");
            Console.WriteLine("Credit [1]");
            Console.WriteLine("Debit  [2]");
            Console.WriteLine("Cash   [3]");
            
            //  Loop that runs as long as the user choose an invalid payment method
            bool purchase = false;
            do
            {
                string input = Console.ReadLine();
                if (input == "1")
                {
                    credit.Pay(price);
                    purchase = true;
                }
                else if (input == "2")
                {
                    debit.Pay(price);
                    purchase = true;
                }
                else if (input == "3")
                {
                    cash.Pay(price);
                    purchase = true;
                }
                else
                {
                    Console.WriteLine("Sorry, thats not an option, please choose a payment method");
                }
            } while (!purchase);
        }
    }
}

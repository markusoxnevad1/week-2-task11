﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task11
{
    //  Creates a saving card for the customer to use, subclass of payment
    public class Debit : Payment
    {
        //  Method that allow the customer to pay for an item
        //  but not if the item's price, exceed the card balance
        public int Pay(int amount)
        {
            if (amount <= balance)
            {
                balance -= amount;
                Console.WriteLine("Congratulations, you successfully bought this item");
            }
            else
            {
                Console.WriteLine("You dont have enough money, please chose another payment method");
            }
            return balance;
        }
    }
}

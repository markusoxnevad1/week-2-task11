﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task11
{
    //  Subclass of Payment class, inherit balances
    public class Cash : Payment
    {
        //  Pay method, used to pay for an item
        public double Pay(int amount)
        {
            if (amount <= balance)
            {
                //  Creates a scenario where the customer pays for an item using
                //  all the cash he has, then returns the change in 100kr notes and single coins
                int change = balance - amount;
                List<Note> notes = new List<Note>();
                List<Coin> coins = new List<Coin>();
                int coinAmount = change % 100;
                int noteAmount = (change - coinAmount)/100;
                for (int i = 0; i < coinAmount; i++)
                {
                    coins.Add(new Coin());
                }
                for (int i = 0; i<noteAmount; i++)
                {
                    notes.Add(new Note());
                }
                Console.WriteLine("You gave me " + balance + " in cash");
                Console.WriteLine("therefore you get " + coins.Count + "kr in coins and ");
                Console.WriteLine(notes.Count + "x 100kr note in change, adding up to a");
                Console.WriteLine("total of " + change + " in change");
                Console.WriteLine("Thanks for your purchase!");
                balance -= amount;
            }
            else
            {
                Console.WriteLine("You dont have enough cash, please chose another payment method");
            }
            return balance;
        }
    }
}

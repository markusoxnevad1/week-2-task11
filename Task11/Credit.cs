﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task11
{
    //  Program that creates a credit card, a subclass of payment
    class Credit : Payment
    {
        public int limit;
        //  Method that allows the customer to pay with the credit card, but not exceed the credit card limit
        public int Pay(int amount)
        {
            if (amount <= limit - balance)
            {
                balance += amount;
                Console.WriteLine("Congratulations, you successfully bought this item");
            }
            else
            {
                Console.WriteLine("Your credit card is maxed out, please chose another payment method");
            }
            return balance;
        }
    }
}
